<?php namespace Tekton\Facades;

class Config extends \Tekton\Support\Facade {
    protected static function getFacadeAccessor() { return 'config'; }
}
