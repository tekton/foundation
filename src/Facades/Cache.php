<?php namespace Tekton\Facades;

class Cache extends \Tekton\Support\Facade {
    protected static function getFacadeAccessor() { return 'cache'; }
}
