<?php namespace Tekton\Facades;

class Request extends \Tekton\Support\Facade {
    protected static function getFacadeAccessor() { return 'request'; }
}
