<?php namespace Tekton\Facades;

class File extends \Tekton\Support\Facade {
    protected static function getFacadeAccessor() { return 'files'; }
}
