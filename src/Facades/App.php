<?php namespace Tekton\Facades;

class App extends \Tekton\Support\Facade {
    protected static function getFacadeAccessor() { return 'app'; }
}
